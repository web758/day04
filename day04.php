<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>

    <style>
        .wrapper {
            width: 450px;
            text-align: left;
            border: 2px solid blue;
            padding: 40px 36px;
            align-items: center;
            margin: auto;
        }

        form {
            width: 100%;
        }

        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;

        }

        .form-group-1 {
            margin-bottom: 16px;
            margin-top: 10px;
        }

        .form-group .form-label {
            width: 100px;
            margin-right: 16px;
            background-color: #70AD47;
            border: 1px solid #41719C;
            padding: 10px;
            color: white;
        }

        .form-label span {
            color: #DE2710;
        }

        .form-group .form-input {
            border: 1px solid #41719C;
            width: 260px;
        }

        .check-box {
            align-self: center;
            margin-right: 10px;
        }

        .btn-signup {
            text-align: center;
        }

        button {
            background-color: #70AD47;
            border: 1px solid #41719C;
            width: 150px;
            padding: 12px 10px;
            text-align: -webkit-center;
            margin-top: 20px;
            border-radius: 8px;
            color: white;
        }

        select {
            padding: 0px 20px;
            border: 1px solid #41719C;
        }

        .input-date {
            width: 181px !important;
            padding: 0px 10px;
            color: black !important;
        }

        .error {
            color: red;
        }
    </style>
</head>

<body>
    
    <?php
    $err = array();
    function isDate($string)
    {
        if (preg_match('/^([0-9]{1,2})\\-([0-9]{1,2})\\-([0-9]{4})$/', $string)) {
            return true;
        } else {
            return false;
        }
    }
    if (isset($_POST['signup'])) {

        if (empty($_POST['username'])) {
            $error['username'] = "Hãy nhập họ tên.";
        }
        if (empty($_POST['gender'])) {
            $error['gender'] = "Hãy chọn giới tính.";
        }
        if (empty($_POST['khoa']) || $_POST['khoa'] == 'NULL') {
            $error['khoa'] = "Hãy chọn khoa.";
        }
        if (empty($_POST['birthday'])) {
            $error['birthday'] = "Hãy nhập ngày sinh.";
        }
        if (empty($_POST['address'])) {
            $error['address'] = "Hãy nhập địa chỉ.";
        }
    }
    ?>
    <div class="wrapper">
        <form method="post" action="./day04.php">
            <div class="error"><?php echo isset($error['username']) ? $error['username'] : "" ?></div>
            <div class="error"><?php echo isset($error['gender']) ? $error['gender'] : "" ?></div>
            <div class="error"><?php echo isset($error['khoa']) ? $error['khoa'] : "" ?></div>
            <div class="error"><?php echo isset($error['birthday']) ? $error['birthday'] : "" ?></div>
            <div class="error"><?php echo isset($error['address']) ? $error['address'] : "" ?></div>


            <div class="form-group form-group-1">
                <div class="form-label">Họ và tên <span>*</span></div>
                <input class="form-input" type="text" name="username" id="username">
            </div>
            <div class="form-group">
                <div class="form-label">Giới tính <span>*</span></div>
                <?php
                $gender = array("0" => "Nam", "1" => "Nữ");
                for ($i = 0; $i < count($gender); $i++) {
                    echo '  <div class="check-box">
                            <input type="radio"  name="gender" value=" ' . $i . '">
                            <label for="html">' . $gender[$i] . '</label>
                        </div> ';
                }
                ?>
            </div>
            <div class="form-group ">
                <div class="form-label">Phân khoa <span>*</span></div>
                <select name="khoa" id="khoa">
                    <?php
                    $falcuty = array("NULL" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                    foreach ($falcuty as $key => $value) {
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <div class="form-label">Ngày sinh <span>*</span></div>
                <input id="txtDate" name= "birthday" class="input-date form-input" readonly="readonly" placeholder="dd/mm/yy" />

            </div>

            <div class="form-group">
                <div class="form-label">Địa chỉ <span>*</span></div>
                <input class="form-input" type="text" name="address" id="">
            </div>
            <div class="btn-signup">
                <button class="form-button" type="submit" name="signup">Đăng ký</button>
            </div>
        </form>
    </div>
</body>

</html>